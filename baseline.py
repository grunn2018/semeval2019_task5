#!/usr/bin/python3

# Team Grunn2019
# baseline.py
# --file making baseline
# 25-09-2018
# revisions: 1

from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.svm import LinearSVC, SVC
from sklearn.pipeline import Pipeline
from sklearn.metrics import precision_recall_fscore_support, classification_report, accuracy_score
from nltk.stem import SnowballStemmer, PorterStemmer
from nltk.tokenize import RegexpTokenizer
import nltk

def tokenizer(words):
	tokenizer = RegexpTokenizer(r'\w+')
	text = tokenizer.tokenize(" ".join(words))

	return text

# a dummy function that just returns its input
def identity(x):
	return x

def train_test(xtrain, ytrain, xtest, ytest):
	# we use a dummy function as tokenizer and preprocessor,
	# since the texts are already preprocessed and tokenized.
	vec = TfidfVectorizer(ngram_range=(1, 4), analyzer='word', preprocessor = identity, tokenizer = tokenizer, stop_words = 'english')
	#vec = TfidfVectorizer(ngram_range=(2, 4), analyzer='char', preprocessor = identity, tokenizer = tokenizer, stop_words = 'english')
	#vec = CountVectorizer(ngram_range=(1, 4), analyzer='word', preprocessor = identity, tokenizer = tokenizer, stop_words = 'english')

	# combine the vectorizer with a classifier
	"""classifier = Pipeline( [('vec', vec),
							('cls', LinearSVC(weight='balanced'))] )"""
	classifier = Pipeline( [('vec', vec),
							('cls', SVC(probability=True))] )

	# fit the traindata and the trainlabels to the classifier
	classifier.fit(xtrain, ytrain)

	# get the predicted y's(labels)
	yguess = classifier.predict(xtest)

	#https://stackoverflow.com/questions/15015710/how-can-i-know-probability-of-class-predicted-by-predict-function-in-support-v
	results = classifier.predict_proba(xtest)
	print(results)

	# compare yguess with ytest, calculate and return results
	accuracy = accuracy_score(ytest, yguess)
	precision, recall, f1score, support = precision_recall_fscore_support(ytest, yguess, average="weighted")
	report = classification_report(ytest, yguess)

	return accuracy, precision, recall, f1score, support, report
