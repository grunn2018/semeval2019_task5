#!/usr/bin/python3
#readdata.py

# read hatespeech corpus, append tweets and label
# class 1 corresponds as hatespeech, 0 not

import re
import csv
import string
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer, PorterStemmer

stemmer = SnowballStemmer("english")
# stemmer = PorterStemmer()

def clean_text(text):
	# remove = [r"http\S+", r"@\S+", r"#SemST", r"#", r"\"", r"\.\S", r"\d+"]
	remove = [r"//t\S+", r"\d+", r"http\S+"]
	for item in remove:
		text = re.sub(item, "", text)
	return text

def preprocess_data(tokens):
	stopwords_list = stopwords.words("english")
	whitelist = ["why", "not", "no", "n't", "against", "!", "?"]
	quotechar_list = ['"', '""', "''", "'", '``', '“']
	tokens = [word for word in tokens if (word not in stopwords_list or word in whitelist)]
	tokens = [word for word in tokens if (word not in list(string.punctuation) or word in whitelist)]
	# tokens = [word for word in tokens if (word not in quotechar_list)]
	return tokens

def read_corpus(corpus_file):
	documents = []
	labels = []
	count = 0
	with open(corpus_file, encoding='utf-8') as f:
		rd = csv.reader(f, delimiter='\t', quotechar="'")
		next(rd)	# skip first line
		for line in rd:
			tokens = line[1]
			tokens = clean_text(tokens)
			tokens = word_tokenize(tokens)
			tokens = [x.lower() for x in tokens]
			tokens = preprocess_data(tokens)
			tokens = [stemmer.stem(token) for token in tokens if (stemmer.stem(token) != '')]
			labels.append(line[3])
			documents.append(tokens)
			
			count += 1
			# uncomment for testing
	# 		if count > 50:
	# 			break
	# print(documents)
	# exit()

	return documents, labels