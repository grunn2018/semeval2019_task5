
#!/usr/bin/python3

# Team Grunn2019
# main.py
# --file to start classification
# 25-09-2018
# revisions: 1

# command: python3 main.py
# up-and-coming: argparse for arguments.

from read_data import read_corpus
from baseline import train_test
from BiLSTM import biLSTM

if __name__ == '__main__':

    # Read in the data and embeddings
    xtrain, ytrain = read_corpus("public_development_en/train_en.tsv")
    xtest, ytest = read_corpus("public_development_en/dev_en.tsv")

    #print(xtrain, ytrain)

    #accuracy, precision, recall, f1score, support, report = train_test(xtrain, ytrain, xtest, ytest)

    # Create model for BiLSTM ONLY HAVE TO RUN THIS ONCE
    biLSTM(xtrain, ytrain, xtest, ytest)

    # Get BiLSTM output on testset
    pred_BiLSTM = outputBiLSTM(xtest)
    print(pred_BiLSTM)

    # Run ensemble with predictions from all classifiers

    print('Done!!!!!!')
    # print  accuracy and f1_score
    #print("## accuracy and f1_score:")
    #print("accuracy: {:.3}".format(accuracy))
    #print("f1_score: {:.3}".format(f1score))

    #print("\n## classification report:")
    #print(report)







